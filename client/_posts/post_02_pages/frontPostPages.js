// Post main ////////////////////////////////////////////

// show all quotes per page
Template.frontPostPages.created = function() {
	// Needed on URL entry, otherwise select in list takes care
	Session.set('currentId', nav.param1);
}

Template.frontPostPages.helpers({
	group_: function() {
		Session.set('currentPostGroup', '_pagesGroup');
		return;
	},
	page: function() {
		return getPageSlugBase();
	},
	group: function() {
		return getPostGroupSlug();
	},
	slug1: function() {
		return nav.param1;
	}
});


// Blog ///////////////////////////////////////////////////

// List of posts

Template.frontBlogList.helpers({
	list: function() {
		var page = getPageSlugBase();
		var group = getBlogGroupSlug();
		var id = Posts.find({'page': page, 'groupSlug': group}, {sort: {timeStamp: -1}}).fetch();
		var list = [];
		for(var i = 0; i < id.length; i++) {
			var item = {};
			item.slug = id[i].slug;
			item.title = id[i].title;
			item.href = getPageSlugBase();
			item.href = page;
			list.push(item);
		}
		return list;
	},
	listSelected: function() {
		return Session.equals('currentId', this.slug) ? 'active' : '';
	}
});

Template.frontBlogList.events({
	'click .listBtn': function(e){											// Select in list
		e.preventDefault();
		Session.set('currentId', this.slug);
		Session.set('currentBlogGroupId', '');


		// Use only first language only slugs
		Router.go(getPageSlugBase() + '_slug1', {x: this.slug});


	}
});



// Post Slug 1

Template.frontBlogSlug1.created = function() {
	// Needed on URL entry, otherwise select in list takes care
	Session.set('currentId', nav.param1);
	Session.set('currentBlogGroupId', '');
}

Template.frontBlogSlug1.helpers({
	item: function() {
		var slugx = Session.get('currentId');
		var page = getPageSlugBase();
		var group = getBlogGroupSlug();
		var id = Posts.find({'page': page, 'groupSlug': group, 'slug': slugx});
		setSEOSlug();
		// if (id.count() == 0){
			// id = [];
		// } else {
			// setSEOSlug();
		// }
		return id;
	},
	postFound: function() {
		var slugx = Session.get('currentId');
		var page = getPageSlugBase();
		var group = getBlogGroupSlug();
		var id = Posts.find({'page': page, 'groupSlug': group, 'slug': slugx});
		var ret = (id.count() == 1) ? true : false;
		// clientLog('found = ' + ret);
		return ret;
	},
	page: function() {
		return getPageSlugBase();
	},
	group: function() {
		return getBlogGroupSlug();
	},
	slug1: function() {
		// return nav.param1;
		return Session.get('currentId');
	}
});

Template.frontBlogSlug1.events({
});
