// Group edit //////////////////////////////////////////////////////////////

Template.cmsPostGroupEdit_.helpers({

	item: function() {
		Session.set('edited', false);
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();

		// var id = Posts.find( {_id: Session.get('currentId'), page: p, groupSlug: g, language: l} );
		var id = Posts.find( {'_id': Session.get('currentBlogGroupId'), 'page': p, 'groupSlug': g, 'language': l} );

		// clientLog('count = ' + id.count());

		if (id.count() == 0){ 		// If no result, pick first item
			// id = Posts.find( {'page': p, 'language': l}, {sort: {timeStamp: -1}, limit: 1} );
			id = Posts.find( {'page': p, 'groupSlug': g, 'language': l}, {sort: {timeStamp: -1}, limit: 1} );
			// id = Posts.find( {'page': p, 'groupSlug': g, 'language': l} );
		}
		if (id.count() > 0){ 			// Pick first result
			Session.set('currentBlogGroupId', id.fetch()[0]._id);
		}

		return id;
	},

	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update group', classActive: classActive};
	}
});


Template.cmsPostGroupEdit_.events({
	'keydown #main-item': function(e) {									// Check if user edits anything
		Session.set('edited', true);
	},
	'click #update': function(e) {											// Update item
		e.preventDefault();
		// If been edited then update
		if ( Session.get('edited') ) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'groupName': 	$('#input_title').val(),
				//'seoTitle': 	$('#input_seoTitle').val(),
				//'seoDescr': 	$('#input_seoDescr').val()
			}});
			Session.set('edited', false);
		}
	}
});
