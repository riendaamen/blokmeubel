// Groups

Template.frontPostPagesGrouped.created = function() {
	// Needed on URL entry, otherwise select in list takes care
	Session.set('currentId', nav.param2);
}

Template.frontPostPagesGrouped.helpers({
	group_: function() {
		Session.set('currentPostGroup', nav.param1);
		return;
	},

	slug1: function() {
		return nav.param1;
	},
	slug2: function() {
		return nav.param2;
	}

});


// List of groups ////////////////////////////////////////////////

// Template.frontBlogGroupList.created = function() {
	// setSEOGroup();
// }

Template.frontBlogGroupList.helpers({
	list: function() {
		var page = getPageSlugBase();
		var id = BlogGroups.find({'page': page}, {sort: {timeStamp: -1}}).fetch();
		var list = [];
		for(var i = 0; i < id.length; i++) {
			var item = {};
			item.urlBase 		= page;
			item.groupSlug 	= id[i].groupSlug;
			item.groupName 	= id[i].groupName;
			list.push(item);
		}
		//setSEOGroup();
		return list;
	},
	listSelected: function() {
		// return Session.equals('currentId', this.slug) ? 'active' : '';
		return Session.equals('currentBlogGroupId', this.groupSlug) ? 'active' : '';
	}
});

Template.frontBlogGroupList.events({
	'click .listBtn': function(e){											// Select in list
		e.preventDefault();
		Session.set('currentBlogGroupId', this.groupSlug);
		//clientLog('Select post = ' + this.slug);

		// Use only first language only slugs
		Router.go(getPageSlugBase() + '_slug1', {x: this.groupSlug});
				// setSEOGroup();

	}
});


// List of posts

Template.frontBlogGroupSlug1.created = function() {
	Session.set('currentBlogGroupId', nav.param1);
}

Template.frontBlogGroupSlug1.helpers({
	list: function() {
		var page = getPageSlugBase();
		// var group = getBlogGroupSlug();
		var group = Session.get('currentBlogGroupId');
		var id = Posts.find({'page': page, 'groupSlug': group}, {sort: {timeStamp: -1}}).fetch();
		var list = [];
		for(var i = 0; i < id.length; i++) {
			var item = {};
			item.urlBase 		= page;
			item.groupSlug 	= group;
			item.slug 			= id[i].slug;
			item.title 			= id[i].title;
			list.push(item);
		}
		setSEOGroup();
		return list;
	},
	listSelected: function() {
		return Session.equals('currentId', this.slug) ? 'active' : '';
	}
});

Template.frontBlogGroupSlug1.events({
	'click .listBtn': function(e){											// Select in list
		e.preventDefault();
		Session.set('currentId', this.slug);

		// Use only first language only slugs

		// Router.go(getPageSlugBase() + '_slug1', {x: this.slug});
		// clientLog('goto ' + getPageSlugBase() + '/' + this.groupSlug + '/' + this.slug);
		// clientLog('goto ' + this.urlBase + '/' + this.groupSlug + '/' + this.slug);
		Router.go(getPageSlugBase() + '_slug2', {x: this.groupSlug, y:this.slug});
	}
});


// Post Slug 2

Template.frontBlogGroupSlug2.created = function() {
	Session.set('currentId', nav.param2);
}

Template.frontBlogGroupSlug2.helpers({
	item: function() {
		var slugx = Session.get('currentId');
		var page = getPageSlugBase();
		var group = Session.get('currentBlogGroupId');
		var id = Posts.find({'page': page, 'groupSlug': group, 'slug': slugx});
		setSEOSlug();

		// if (id.count() == 0){
			// id = [];
		// } else {
			// setSEOSlug();
			// setSEOSlug2();
		// }

		return id;
	},
	postFound: function() {
		var slugx = Session.get('currentId');
		// var page = getPageSlugBase();
		// var group = getBlogGroupSlug();
		var page = getPageSlugBase();
		var group = Session.get('currentBlogGroupId');

		var id = Posts.find({'page': page, 'groupSlug': group, 'slug': slugx});
		var ret = (id.count() == 1) ? true : false;
		return ret;
	},
	page: function() {
		return getPageSlugBase();
	},
	group: function() {
		// return getBlogGroupSlug();
		return Session.get('currentBlogGroupId');
	},
	slug: function() {
		// return nav.param1;
		return Session.get('currentId');
	}
});

Template.frontBlogGroupSlug2.events({
});
