// Post Agenda Special Tabs ////////////////////////////////////////////////////

cmsPostTabListAboutUs = [
	{tabName: 'post', 		name: 'Quote'},
	{tabName: 'SEO', 			name: 'SEO'}
];

Session.set('currentPostTabAboutUs', cmsPostTabListAboutUs[0].tabName);

Template.cmsPostAboutUs.helpers({
	list: function() {
		var list = [];
		var id = cmsPostTabListAboutUs;
		for(var i = 0; i < id.length; i++) {
			var item = {};
			item.name = 			id[i].name;
			item.tabName = 		id[i].tabName;
			list.push(item);
		}
		return list;
	},
	listSelected: function() {
		return Session.equals('currentPostTabAboutUs', this.tabName) ? 'active' : '';
	},
	currentTab: function() {
		return Session.get('currentPostTabAboutUs');
	},
	item: function() {
		Session.set('edited', false);
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();

		// It seems that the exception for Agenda is not really needed
		var id = Posts.find( {_id: Session.get('currentId'), page: p, groupSlug: g, language: l} );

		if (id.count() == 0){ 		// If no result, pick first item
			id = Posts.find( {page: p, groupSlug: g, language: l}, {sort: {order: 1}, limit: 1} );
		}
		if (id.count() > 0){ 			// Pick first result
			Session.set('currentId', id.fetch()[0]._id);
		}
		return id;
	},
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsPostAboutUs.events({
	'click .listBtn': function(e){															// Select postTab from list
		e.preventDefault();
		Session.set('currentPostTabAboutUs', this.tabName);
		Session.set('edited', false);
	},
	'keydown #main-item': function(e) {
		Session.set('edited', true);
	},
	'click .checkbox' : function(e) {
		Session.set('edited', true);
	},
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			// clientLog('update, id = ' + this.buttonId);

			if (Session.get('currentPostTabAboutUs') == 'post') {
				Posts.update( {'_id': this.buttonId}, {$set: {
					'title':								$('#input_title').val(),
					'quote':								$('#input_quote').val(),
					'person':								$('#input_person').val(),
					'colF_text':						CKEDITOR.instances.input_colF_text.getData()
				}});
			}

			if (Session.get('currentPostTabAboutUs') == 'SEO') {
				Posts.update( {'_id': this.buttonId}, {$set: {
					'seoTitle':							$('#input_seoTitle').val(),
					'seoDescr':							$('#input_seoDescr').val()
				}});
			}

			Session.set('edited', false);
		}
	}
});
