// front postTab ////////////////////////////////////////////////////////

// show all quotes per page

Template.frontPostAboutUs.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var s = Session.get('currentId');

		if (g == '_mainGroup') {
			s = '_main';
		}
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': '_mainGroup', 'slug': '_main'});
		getSEO_fromPost(post);
		return post;
	},
	postFound: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var s = Session.get('currentId');
		if (g == '_mainGroup') {
			s = '_main';
		}
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': '_mainGroup', 'slug': '_main'});
		var ret = (post.count() == 1) ? true : false;
		return ret;
	},
	page: function() {
		return getPageSlugBase();
	},
	group: function() {
		// Session.set('currentPostGroup', '_mainGroup');
		// return Session.get('currentPostGroup');
		return Session.get('navParam1');
	},
	show_quote_row2: function() {
		var ret = (this.quote_text_row2 == '') ? false : true;
		return ret;
	},
	image1: function() {
		return Images.find( {'_id': this.imageIntro } );
	},
	image2: function() {
		return Images.find( {'_id': this.imageColumnR } );
	}
});


// opsomming extLinks //////////////////////////////////////////////////////////////

Template.frontPostAboutUsQuote.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = Session.get('navParam1');
		posts = Posts.find( {page: p, groupSlug: g, language: l}, {sort: {order: 1}, skip: 3} );
		return posts;
	},
	groupName: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_groupsGroup'; //getPostGroupSlug();
		var s = nav.param1;
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		//clientLog('grpNAme = ' + post.fetch()[0].title);
		return post.fetch()[0].title;
	}

});



Template.frontPostAboutUsQuote.events({
	'click .btnQuote': function(e){																// Select quote
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		var slugX = nav.param1;
		var slugY = this.slug;
		clientLog ('routerName = ' + routerName + ' - ' + slugX + ' - ' + slugY );
		Router.go(routerName + '_slug2', {x: slugX, y: slugY});
	}
});
