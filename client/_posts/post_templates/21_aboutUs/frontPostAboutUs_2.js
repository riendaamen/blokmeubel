// show 1 quote, main page

Template.frontPostAboutUs_2.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var s = Session.get('currentId');
		if (g == '_mainGroup') {
			s = '_main';
		}
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': '_mainGroup', 'slug': '_main'});
		getSEO_fromPost(post);
		return post;
	},
	postFound: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var s = Session.get('currentId');
		if (g == '_mainGroup') {
			s = '_main';
		}
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': '_mainGroup', 'slug': '_main'});
		var ret = (post.count() == 1) ? true : false;
		return ret;
	},
	page: function() {
		return getPageSlugBase();
	},
	group: function() {
		// Session.set('currentPostGroup', '_mainGroup');
		return Session.get('currentPostGroup');
	},
	show_quote_row2: function() {
		var ret = (this.quote_text_row2 == '') ? false : true;
		return ret;
	},
	image1: function() {
		return Images.find( {'_id': this.imageIntro } );
	},
	image2: function() {
		return Images.find( {'_id': this.imageColumnR } );
	}
});


// show 1 quote //////////////////////////////////////////////////////////////

Template.frontPostAboutUsQuote_2.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = Session.get('navParam1');
		var s = Session.get('navParam2');
		posts = Posts.find( {page: p, groupSlug: g, language: l, slug: s}, {sort: {order: 1}} );
		return posts;
	},
	page: function() {
		return getPageSlugBase();
	},
	group: function() {
		return Session.get('navParam1');
	},
	pageSlug: function() {
		return Session.get('navParam2');
	},
	groupName: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_groupsGroup'; //getPostGroupSlug();
		// var s = Session.get('currentPostGroup');
		var s = nav.param1;
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		//clientLog('grpNAme = ' + post.fetch()[0].title);
		return post.fetch()[0].title;
	}
});


Template.frontPostAboutUsQuote_2.events({
	'click .btnQuote': function(e){																// Select quote
		e.preventDefault();
		// clientLog ('Goto quote = ' + nav.param1 );
		// var url = Router.current().url + '/' + this.slug;
		// Session.set('currentId', this.slug);
		// Session.set('currentBlogGroupId', '');
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		var slugX = nav.param1;
		var slugY = this.slug;
		clientLog ('routerName = ' + routerName + ' - ' + slugX + ' - ' + slugY );
		Router.go(routerName + '_slug2', {x: slugX, y: slugY});
	}
});
