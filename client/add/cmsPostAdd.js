//	Dialog Add Post ///////////////////////////////////////////////////////////////

Template.cmsBlogAdd.helpers({
	bntOkayStatus: function() {
		return SlugStatus.find().fetch()[0].btnOkayStatus;
	}
});

Template.cmsBlogAdd.events({
	'click .btnCancel': function(e) {											// Cancel
		Session.set('addDialogShow', false);
	},
	'click #btnOkay': function(e) {												// Okay
		if (SlugStatus.find().fetch()[0].correct) {
			var slug = SlugStatus.find().fetch()[0].slug;
			var title = SlugStatus.find().fetch()[0].title;
			addPostCheckLanguage(slug, title);
			Session.set('addDialogShow', false);
		}
	},
	'keypress #modal-one': function(e) {									// Enter key
		if (e.keyCode == 13) {
			if (SlugStatus.find().fetch()[0].correct) {
				var slug = SlugStatus.find().fetch()[0].slug;
				var title = SlugStatus.find().fetch()[0].title;
				addPostCheckLanguage(slug, title);
				Session.set('addDialogShow', false);
			}
		}
	}
});


addPostCheckLanguage = function(slug, title) {
	var language = Session.get('cmsLanguage');
	// if( getPageSlugBase() == 'actueel.agenda') { 	// mono language
		// language = -1;
	// }
	addPost(slug, title, language);
};


//	Add post //////////////////////////////////////////////////////////////////////
//		language	-1		allways use first language and URL, like Agenda
//		language	x 		language x, also different routes
//
addPost = function(slug, title, language) {							// Add post

	// clientLog ('Add post');
	// clientLog ('__urlBase = '		+	getPageSlugBase() );
	// clientLog ('__groupSlug = '	+	getPostGroupSlug() );

	var mDate = new Date();

	// create initial sizes for tuinhaard
	var sizesList = [];
	var p = 0;
	p = p.toFixed(2);

	for (var i=0; i<3; i++) {
		sizesList.push({
			'name': 			'Vul een maat in.',
			'priceEuro': 	0,
			'priceCent': 	i,
			'price':			p
		});
	}


	// add ////////////////////////////////////////////////////////////////

	var id = Posts.insert({
		timeStamp: 			(new Date()).getTime(),
		date: 					mDate,
		dateNice: 			dateNice2(mDate),
		order:					90,
		language:				language,
		page:						getPageSlugBase(),
		groupName:			title,
		groupSlug:			getPostGroupSlug(),
		slug: 					slug,
		title:					title,
		slideList:			[],
		sizesList:			sizesList,		// size list for tuinhaard
		sizesListCount:	3,						// number of sizes for tuinhaard
		variationList:	[],
		hiliList:				[],
		article:				'New text.',
		seoTitle:				'BLOK meubel | design meubel veranda keuken interieur kachel',
		seoDescr:				'BLOK maakt en ontwerpt o.a. unieke interieurs, design meubels, keukens, eikenhouten veranda’s, robuuste stalen buitenkachels'
	});
	Session.set('currentId', id);
	listMoveOrder();
};


// Local Collection for storing status based on user input
//	checks on:	- empty field
//							- URL allready exists (double entries)
SlugStatus = new Mongo.Collection(null);
SlugStatus.insert({message: 'init'});

slugStatusUpdate = function(slug) {
	var correct = true;
	var message = '&nbsp;';
	var messageCorrect = true;

	// Check if empty
	if (slug.length == 0){
		correct = false;
		message = 'Please enter a title.';
	}

	// Check if allready exists

	// var page = getPageSlugBase();
	// var group = getPostGroupSlug();
	// var id = Posts.find({'page': page, 'groupSlug': group, 'slug': slug});

	var l = Session.get('cmsLanguage');
	var p = getPageSlugBase();
	var g = getPostGroupSlug();
	var id = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': slug});

	if( p == 'actueel.agenda') { 	// execption for Agenda
		var id = Posts.find({'page': p, 'groupSlug': g, 'slug': slug});
	} else {
		var id = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': slug});
	}

	if(id.count() > 0){
		correct = false;
		messageCorrect = false;
		message = "Sorry, the URL for this title is allready in use by another post.<br/>Please fill in a unique title.";
	}

	// Set btnOkayStatus
	var btnOkayStatus = (correct) ? 'active' : 'disabled';

	var id = SlugStatus.find({});
	SlugStatus.update({'_id': id.fetch()[0]._id}, {$set: {
		'correct': 				correct,
		'title':					$('#dialog-input-title').val(),
		'btnOkayStatus': 	btnOkayStatus,
		'message': 				message,
		'messageCorrect': messageCorrect
	} });
}



titleToSlug = function() {
	var str = $('#dialog-input-title').val();
	str = jQuery.trim(str) // Trimming recommended by Brooke Dukes - http://www.thewebsitetailor.com/2008/04/jquery-slug-plugin/comment-page-1/#comment-23
								.replace(/\s+/g,'-').replace(/[^a-zA-Z0-9\-]/g,'').toLowerCase() // See http://www.djangosnippets.org/snippets/1488/
								.replace(/\-{2,}/g,'-'); // If we end up with any 'multiple hyphens', replace with just one. Temporary bugfix for input 'this & that'=>'this--that'
	var id = SlugStatus.find({});
	SlugStatus.update({'_id': id.fetch()[0]._id}, {$set: {'slug': str} });
	return str;
}


Template.cmsBlogAddTitle.rendered = function(){
	$('#dialog-input-title').focus();		// Set focus to input
};
Template.cmsBlogAddTitle.events({
	'keyup #dialog-input-title': function(e) {
		var slug = titleToSlug();
		slugStatusUpdate(slug);
	}
});


Template.cmsBlogAddTitleStatus.rendered = function(){
		var slug = titleToSlug();
		slugStatusUpdate(slug);
};
Template.cmsBlogAddTitleStatus.helpers({
	status: function() {
		return SlugStatus.find();
	}
});


Template.cmsBlogAddSlug.rendered = function(){
	//slugStatusUpdate();
};
Template.cmsBlogAddSlug.helpers({
	slug: function() {
		return SlugStatus.find().fetch()[0].slug;
	}
});


function dateNice2(date) {
	var y = date.getFullYear();
	var d = date.getDate();
	var month=new Array();
	month[0]="januari";
	month[1]="februari";
	month[2]="maart";
	month[3]="april";
	month[4]="mei";
	month[5]="juni";
	month[6]="juli";
	month[7]="augustus";
	month[8]="september";
	month[9]="oktober";
	month[10]="november";
	month[11]="december";
	var m = month[date.getMonth()];
	var nice = d + ' ' + m + ' ' + y;
	// clientLog ('dateNice = ' + nice);
	return nice;
}
