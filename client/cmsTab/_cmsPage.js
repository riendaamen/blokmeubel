Template.cmsPage.helpers({
	cmsEditName: function() {

		// must check length to see if component s available
		// clientLog('length nav.cmsTabs = ' + nav.cmsTabs.length);
		// clientLog('Session.get(navCmsTab) = ' + Session.get('navCmsTab') );

		if (Session.get('navCmsTab') < nav.cmsTabs.length) {
			var navCmsTab = nav.cmsTabs[Session.get('navCmsTab')].component;
			return navCmsTab;
		} else {
			return;
		}

	}
});


// SpaceBars helper
UI.registerHelper('is_equal', function(value1, value2) {
	if (value1 === value2) {
		return true;
	} else {
		return false;
	}
});
