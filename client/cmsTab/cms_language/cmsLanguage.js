// CMS

Template.cmsLanguage.helpers({
	buttons: function() {
		var list = [];
		for (var i = 0; i < languages.length; i++) {
			var item = {};
			var sel = "";
			if (i == Session.get('cmsLanguage')) { 
				sel = 'active'; 
			}
			item.buttonName = languages[i].nameLong;
			item.index = i;
			item.classSelected = sel;
			list.push(item);
		}
		return list;
	}
});


// Choose language in cms
Template.cmsLanguage.events({
	'click .btn': function(e) {	
		e.preventDefault();
		var i = this.index;
		// clientLog('select CMS language');
		// Session.set('language', i)		// Makes 'view website' in sync
																		// got a new function
		Session.set('cmsLanguage', i)
	}
});

