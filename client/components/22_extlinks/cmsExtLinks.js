// cms extLinks ////////////////////////////////////////////////////

Template.cmsExtLinksEdit.helpers({
	item: function() {
		Session.set('edited', false);
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		// It seems that the exception for Agenda is not really needed
		var id = Posts.find( {_id: Session.get('currentId'), page: p, groupSlug: g, language: l} );
		if (id.count() == 0){ 		// If no result, pick first item
			id = Posts.find( {page: p, groupSlug: g, language: l}, {sort: {order: 1}, limit: 1} );
		}
		if (id.count() > 0){ 			// Pick first result
			Session.set('currentId', id.fetch()[0]._id);
			Session.set('currentBtnLinkType', id.fetch()[0].linkType);										

		}
		return id;
	},
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	},
	
	linkTypeAct0: function() {
		var ret = (Session.get('currentBtnLinkType') == 0) ? 'active' : '';
		return ret;										
	},
	linkTypeAct1: function() {
		var ret = (Session.get('currentBtnLinkType') == 1) ? 'active' : '';
		return ret;										
	},
	linkType: function() {
		var ret = Session.get('currentBtnLinkType');
		return ret;										
	}
	
	
	
	
});


Template.cmsExtLinksEdit.events({	
	'keydown #main-item': function(e) {
		Session.set('edited', true);										
	},
	'click .checkbox' : function(e) {
		Session.set('edited', true);										
	},
	
	
	'click .btnLinkType0' : function(e) {
		Session.set('currentBtnLinkType', 0);										
		Session.set('edited', true);										
	},
	'click .btnLinkType1' : function(e) {
		Session.set('currentBtnLinkType', 1);										
		Session.set('edited', true);										
	},
	
	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
				Posts.update( {'_id': this.buttonId}, {$set: {
					'title':			$('#input_title').val(),
					// 'linkType':		$('#input_linkType').val(),
					'linkType':		Session.get('currentBtnLinkType'),
					'linkUrl':		$('#input_linkUrl').val()
				}});
			Session.set('edited', false);
		}
	}
});

// <input id='input_title' value={{title}} class='input-small' type='text' maxlength='120'>
// <input id='input_linkType' value={{linkType}} class='input-small' type='text' maxlength='120'>
// <input id='input_linkUrl' value={{linkUrl}} class='input-small' type='text' maxlength='120'>

	
	
// upload /////////////////////////////////////////////////////////////

Template.cmsExtLinksEdit_upload_1.helpers({
	file: function() {
		return DB_file_article.find( {'_id': this.extLinkFile } );
	}
});

Template.cmsExtLinksEdit_upload_1.events({
  'change #fileUploader': function(event, template) {
		clientLog('upload file');
		
    var files = event.target.files;
		var postId = this._id;
    for (var i = 0, ln = files.length; i < ln; i++) {
      DB_file_article.insert(files[i], function (err, fileObj) {
				Posts.update( {'_id': postId}, {$set: {
					'extLinkFile':	fileObj._id
				}});
      });
    }
  },
	'click .red': function() {
		// var r=confirm("Delete this image?");
		// if (r==true){
			DB_file_article.remove( {_id:this._id} );
		// }
	}
});



