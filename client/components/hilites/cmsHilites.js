// hilites

Template.cmsHilites.helpers({
	images: function() {
		return Images.find({}, {sort: {uploadedAt: -1}});
	}
});

Template.cmsHilites.events({
  'change #fileUploader': function(event, template) {
		clientLog('image starting...');
    var files = event.target.files;
    for (var i = 0, ln = files.length; i < ln; i++) {
      Images.insert(files[i], function (err, fileObj) {
				clientLog('image inserted, id= ' + fileObj._id);
        //Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
      });
    }
  },
	'click .red': function() {
		// clientLog('delete ' + this._id);
		// var r=confirm("Delete this image?");
		// if (r==true){
			Images.remove( {_id:this._id} );
		// }
	}
});
