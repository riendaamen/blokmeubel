
// All groups //////////////////////////////////////////////////////////////

Template.opsomming_aboutUs.helpers({
	group: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_groupsGroup';
		groups = Posts.find( {page: p, groupSlug: g, language: l}, {sort: {timeStamp: 1}} );
		return groups;
	},
	language: function() {
		return Session.get('cmsLanguage');
	}

});

Template.opsomming_aboutUs.events({
	'click .btnGroup': function(e){																// Select group (meer ervaringen)
		e.preventDefault();
		clientLog ('meer = ' + this.slug);
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		clientLog ('routerName = ' + routerName );
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});


// the first 3 quotes per group //////////////////////////////////////////////////////////////

Template.opsomming_aboutUsQuote.helpers({
	item: function(parentContext) {
		//clientLog('parentContext C = ' + parentContext.title);
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = parentContext.slug;																	// the trick for getting parent data !!!
		posts = Posts.find( {page: p, groupSlug: g, language: l}, {sort: {order: 1}, limit: 3} );
		return posts;
	},
	language: function() {
		return Session.get('cmsLanguage');
	}

});

Template.opsomming_aboutUsQuote.events({
	'click .btnQuote': function(e){																// Select quote
		e.preventDefault();
		clientLog ('goto = ' + Template.parentData(1).slug );				// get parentData !!!
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		var slugX = Template.parentData(1).slug;
		var slugY = this.slug;
		clientLog ('routerName = ' + routerName + ' - ' + slugX + ' - ' + slugY );
		Router.go(routerName + '_slug2', {x: slugX, y: slugY});
	}
});
