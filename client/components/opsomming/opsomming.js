
// opsomming coaching ///////////////////////////////////////////////////////////////

Template.opsomming_coaching.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var item = Posts.find( {'language': l, page: p, groupSlug: '_pagesGroup'}, {sort: {order: 1}} );
		return item;
	}
});

Template.opsomming_coaching.events({
	'click .btnList': function(e){												// Select in list
		e.preventDefault();
		var url = Router.current().url + '/' + this.slug;
		Session.set('currentId', this.slug);
		Session.set('currentBlogGroupId', '');
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});


// opsomming extLinks //////////////////////////////////////////////////////////////

Template.opsomming_extLinks.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var item = Posts.find( {'language': l, page: p, groupSlug: '_pagesGroup'}, {sort: {order: 1}} );
		// check if url or file
		return item;
	}
});

Template.opsomming_extLinks.events({
	'click .btnList': function(e){												// Select in list
		e.preventDefault();
		var url = Router.current().url + '/' + this.slug;
		Session.set('currentId', this.slug);
		Session.set('currentBlogGroupId', '');
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});

Template.opsomming_extLinksFile.helpers({
	file: function() {
		return DB_file_article.find( {'_id': this.extLinkFile } );
	}
});
