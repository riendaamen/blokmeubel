// SEO

Template.cmsSEO.helpers({
	item: function() {
		var pageIndex = getPageIndex();
		var id = Seos.find( {'page': pageIndex}, {} );
		if(id.fetch().length == 0) {
			insertSeo(pageIndex);												// Insert seo if not present
		}
		return id;
	},
	updateStatus: function() {
		var flag = Session.get('edited');
		var classActive = (flag) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update text', classActive: classActive};
	}
});



Template.cmsSEO.events({
	'keydown #main-item': function(e) {
		Session.set('edited', true);									// Check if user edits these fields
	},
	'click #update': function(e) {									// Update SEO
		e.preventDefault();
		var title = $('#input_title').val();
		var descr = $('#input_descr').val();
		Seos.update( {'_id': this.buttonId}, {$set: {'seoTitle': title, 'seoDescription': descr} } );
		Session.set('edited', false);
	}
});


//
// Insert new Seo
//
insertSeo = function(pageIndex) {
	var iLang = Session.get('language');
	if (nav.cmsMode == true) iLang = Session.get('cmsLanguage');

	var level1 = Session.get('navLevel1');
	var level2 = Session.get('navLevel2');

	var seoTitle = (pages[level1].level2[level2].urlName[iLang]);
	var seoDescr = 'Please enter a description for ' + (pages[level1].level2[level2].urlName[iLang]);

	var	id = Seos.insert({
		'page': 						pageIndex,
		'seoTitle': 				seoTitle,
		'seoDescription': 	seoDescr
	});
	clientLog('Added new document in Seos, id = ' + id);
	return id;
}
