// SEO

Template.frontCompSEO.helpers({
	item: function() {
		var pageIndex = getPageIndex();
		var id = Seos.find( {'page': pageIndex}, {} );
		return id;
	},
	// Check if there is an entry
	state: function() {
		var pageIndex = getPageIndex();
		var id = Seos.find( {'page': pageIndex}, {} );
		var str = (id.fetch().length == 1) ? 'frontCompSEO:' : '! No frontCompSEO yet.';
		return str;
	}
});
