// Text

Template.cmsText.helpers({
	item: function() {
		// Session.set('edited', false);
		var pageIndex = getPageIndex();
		var id = Texts.find( {'page': pageIndex}, {} );
		if (id.fetch().length == 0) {
			insertText(pageIndex);										// Insert text if non present
		} 
		return id;
	},
	updateStatus: function() {
		var flag = Session.get('edited');
		var classActive = (flag) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update text', classActive: classActive};
	}
});


Template.cmsText.events({
	'keydown #main-item': function(e) {						// Check if user edits these fields
		Session.set('edited', true);										
	},
	'click #update': function(e) {								// Update text
		e.preventDefault();
		var title = $('#input_title').val();
		var descr = $('#input_descr').val();
		Texts.update( {'_id': this.buttonId}, {$set: {'title': title, 'article': descr} } );
		Session.set('edited', false);
	}
});


//
// Insert new text
//
insertText = function(pageIndex) {
	var iLang = Session.get('language');
	if (nav.cmsMode == true) iLang = Session.get('cmsLanguage');
	
	var level1 = Session.get('navLevel1');
	var level2 = Session.get('navLevel2');

	var title = (pages[level1].level2[level2].urlName[iLang]);
	var article = 'Please enter a text for ' + (pages[level1].level2[level2].urlName[iLang]);

	var	id = Texts.insert({
		'page': 		pageIndex, 
		'title': 		title,
		'article': 	article
	});
	clientLog('Added new document in Texts, id = ' + id);
	return id;
}
