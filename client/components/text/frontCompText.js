// Text

Template.frontCompText.helpers({
	item: function() {
		var pageIndex = getPageIndex();
		var id = Texts.find( {'page': pageIndex}, {} );
		return id;
	},

	// Check if there is an entry
	state: function() {
		var pageIndex = getPageIndex();
		var id = Texts.find( {'page': pageIndex}, {} );
		var str = (id.fetch().length == 1) ? 'frontCompText:' : '! No text for this page yet, please enter text in CMS.';
		return str;
	}
});
