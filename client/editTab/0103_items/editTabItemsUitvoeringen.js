// item variations //////////////////////////////////////////////////////////////////////////

Session.set('currentVariation', 0);

Template.cmsEdit_itemVariations.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	},
	variationList: function() {
		var list = this.variationList;
		if (Session.get('currentVariation') > (list.length-1)) {
			Session.set('currentVariation', list.length-1);
		}
		var slides = [];
		for (var i = 0; i < list.length; i++) {
			var slide = [];
			slide.imageId = list[i];
			slide.number = i+1;
			slide.index = i;
			slide.active = (i == Session.get('currentVariation')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	slidesShow: function() {
		var post = Posts.find( {_id: this._id} ).fetch()[0];
		var list = post.variationList;
		return (list.length > 0) ? true : false;
	},
	variation: function() {
		return [this.variationList[Session.get('currentVariation')]];
	}
});


Template.cmsEdit_itemVariations.events({	
	'click #update': function(e) {															// Update
		e.preventDefault();
		var post = Posts.find( {_id: this.buttonId} ).fetch()[0];
		var i = Session.get('currentVariation');
		var list = post.variationList;
		var euro = 	$('#inputPriceEuro').val();
		var cent = 	$('#inputPriceCent').val();
		
		list[i].name = 			$('#inputName').val();
		list[i].priceEuro = (euro*1.0);
		list[i].priceCent = (cent*1.0);
		list[i].price = 		((euro*1.0) + (cent/100.0)).toFixed(2);
		
		
		Posts.update( {'_id': this.buttonId}, {$set: {
			'variationList':				list
		}});
	},
	'click .btnSlideNum': function(){													// select
		Session.set('currentVariation', this.index);
	},
  'click .addQuote': function() {														// add
		var postId = this._id;
		var list = this.variationList;
		var p = 0;
		p = p.toFixed(2);
		list.push({
			'name': 			'Vul een naam in.',
			'priceEuro': 	0,
			'priceCent': 	0,
			'price':			p
		});
		
		Posts.update( {'_id': postId}, {$set: {
			'variationList':	list
		}});
		Session.set('currentVariation', list.length-1);
  },
	'click .btnDelSlide': function() {
		var r=confirm("Delete this quote?");
		if (r==true){
			var list = this.variationList;
			list.splice(Session.get('currentVariation'), 1);
			this.variationList = list;
			Posts.update( {'_id': this._id}, {$set: {
				'variationList':	list
			}});
			Session.set('currentVariation', list.length-1);
		}
	},
	'keypress #inputPriceEuro': function(e) {
		// clientLog('key = ' + e.which);
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			$("#errmsg").html("Voer getal in.").show().fadeOut(800);
			return false;
		}
	},
	'keypress #inputPriceCent': function(e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			$("#errmsg").html("Voer getal in.").show().fadeOut(800);
			return false;
		}
	}
});





