// item variations tuinhaard //////////////////////////////////////////////////////////////////////////

Session.set('currentSizeCount_th', 0);
Session.set('currentSize_th', 0);
Session.set('currentVariation_th', 0);

Template.cmsEdit_itemVariations_th.helpers({
	
	sizesCountList: function() {
		var count = this.sizesListCount;
		clientLog('sizesCount = ' + count);
		Session.set('currentSizeCount_th', count);
		var slides = [];
		for (var i = 0; i < 3; i++) {
			var slide = [];
			slide.number = i+1;
			slide.index = i;
			slide.idRoot = this._id;
			slide.active = (i+1 == count) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	
	sizesList: function() {
		var list = this.sizesList;
		// var count = this.sizesListCount;
		var count = Session.get('currentSizeCount_th');
		
		
		// clientLog('sizesList = ' + list);
		if (Session.get('currentSize_th') > (list.length-1)) {
			Session.set('currentSize_th', list.length-1);
		}
		
		if (Session.get('currentSize_th') > (count-1)) {
			Session.set('currentSize_th', count-1);
		}
		
		
		var slides = [];
		// for (var i = 0; i < list.length; i++) {
		for (var i = 0; i < count; i++) {
			var slide = [];
			slide.number = i+1;
			slide.index = i;
			slide.active = (i == Session.get('currentSize_th')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	},
	variationList: function() {
		var list = this.variationList;
		if (Session.get('currentVariation_th') > (list.length-1)) {
			Session.set('currentVariation_th', list.length-1);
		}
		var slides = [];
		for (var i = 0; i < list.length; i++) {
			var slide = [];
			slide.imageId = list[i];
			slide.number = i+1;
			slide.index = i;
			slide.active = (i == Session.get('currentVariation_th')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	slidesShow: function() {
		var post = Posts.find( {_id: this._id} ).fetch()[0];
		var list = post.variationList;
		return (list.length > 0) ? true : false;
	},
	size: function() {
		return [this.sizesList[Session.get('currentSize_th')]];
	},
	variation: function() {
		return [this.variationList[Session.get('currentVariation_th')]];
	}
});


Template.cmsEdit_itemVariations_th.events({	
	'click #update': function(e) {															// Update
		e.preventDefault();
		var post = Posts.find( {_id: this.buttonId} ).fetch()[0];
		
		clientLog('id = ' + this.buttonId);
		
		
		// sizes /////////////////////////////
		var i = Session.get('currentSize_th');
		var list = post.sizesList;
		var euro = 	$('#inputSizePriceEuro').val();
		var cent = 	$('#inputSizePriceCent').val();
		
		list[i].name = 			$('#inputSizeName').val();
		list[i].priceEuro = (euro*1.0);
		list[i].priceCent = (cent*1.0);
		list[i].price = 		((euro*1.0) + (cent/100.0)).toFixed(2);
		
		Posts.update( {'_id': this.buttonId}, {$set: {
			'sizesList':				list
		}});
		
		// options ///////////////////////////
		var i = Session.get('currentVariation_th');
		var list = post.variationList;
		// clientLog(list.length)
		if (list.length > 0) {
			var euro = 	$('#inputPriceEuro').val();
			var cent = 	$('#inputPriceCent').val();
			
			list[i].name = 			$('#inputName').val();
			list[i].priceEuro = (euro*1.0);
			list[i].priceCent = (cent*1.0);
			list[i].price = 		((euro*1.0) + (cent/100.0)).toFixed(2);
			
			Posts.update( {'_id': this.buttonId}, {$set: {
				'variationList':				list
			}});
		}
		
		
	},
	
	
	'click .btnSizeCountNum': function(){											// select sizes count
		clientLog(this.number);
		Session.set('currentSizeCount_th', this.number);
		Session.set('currentSize_th', 0);
		Posts.update( {'_id': this.idRoot}, {$set: {
			'sizesListCount': this.number
		}});
		
		// clientLog('id = ' + this.idRoot);

		
	},
	'click .btnSizeNum': function(){													// select size
		Session.set('currentSize_th', this.index);
	},
	'click .btnSlideNum': function(){													// select option
		Session.set('currentVariation_th', this.index);
	},
  'click .addQuote': function() {														// add
		var postId = this._id;
		var list = this.variationList;
		var p = 0;
		p = p.toFixed(2);
		list.push({
			'name': 			'Vul een naam in.',
			'priceEuro': 	0,
			'priceCent': 	0,
			'price':			p
		});
		
		Posts.update( {'_id': postId}, {$set: {
			'variationList':	list
		}});
		Session.set('currentVariation_th', list.length-1);
  },
	'click .btnDelSlide': function() {
		var r=confirm("Delete this quote?");
		if (r==true){
			var list = this.variationList;
			list.splice(Session.get('currentVariation_th'), 1);
			this.variationList = list;
			Posts.update( {'_id': this._id}, {$set: {
				'variationList':	list
			}});
			Session.set('currentVariation_th', list.length-1);
		}
	},
	'keypress #inputSizePriceEuro': function(e) {
		// clientLog('key = ' + e.which);
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			$("#errmsgSize").html("Voer getal in.").show().fadeOut(800);
			return false;
		}
	},
	'keypress #inputSizePriceCent': function(e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			$("#errmsgSize").html("Voer getal in.").show().fadeOut(800);
			return false;
		}
	},
	
	'keypress #inputPriceEuro': function(e) {
		// clientLog('key = ' + e.which);
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			$("#errmsg").html("Voer getal in.").show().fadeOut(800);
			return false;
		}
	},
	'keypress #inputPriceCent': function(e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			$("#errmsg").html("Voer getal in.").show().fadeOut(800);
			return false;
		}
	}
	
	
	
	
});



