
//
// Make a nice timestamp, like 2014-12-16_131800
//

getSlugTimeStamp = function() {
	var slug = '';
	var txt = '';
	var time = new Date();
	slug = slug + time.getFullYear() + '-';

	txt = '' + (time.getMonth() + 1);
	if (txt.length == 1) txt = '0' + txt;
	slug = slug + txt + '-';

	txt = '' + time.getDate();
	if (txt.length == 1) txt = '0' + txt;
	slug = slug + txt + '_';

	txt = '' + time.getHours();
	if (txt.length == 1) txt = '0' + txt;
	slug = slug + txt;

	txt = '' + time.getMinutes();
	if (txt.length == 1) txt = '0' + txt;
	slug = slug + txt;

	txt = '' + time.getSeconds();
	if (txt.length == 1) txt = '0' + txt;
	slug = slug + txt;

	return slug;
}
