
// List slider ///////////////////////////////////////////////////////////////////////////////////
// 		grp = slider, highlight
// 		slg = ~

Template.cmsList_slider.helpers({
	list: function() {
		// Session.set('currentPostGroup', '_pagesGroup');
		// Session.set('currentPostGroup', 'slider');
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var mainPost = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		return mainPost;
	},
	listSelected: function() {												
		return Session.equals('currentId', this._id) ? 'active' : '';
	},
	deleteStatus: function() {
		var classActive = 'enabled';
		
		// var page = getPageSlugBase();
		// var query = Posts.find( {'_id': Session.get('currentId'), 'page': page} );
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, '_id': Session.get('currentId')} );
		
		if (query.count() == 0){ 		// If no posts, disable delete button
			classActive = 'disabled';
		}
		return {buttonId: Session.get('currentId'), classActive: classActive};
	},
	addDialogShow: function() {												
		return Session.get('addDialogShow');
	},
	upDownShow: function() {
		// return ( getPageSlugBase() == 'actueel.agenda' ) ? false: true; // hide order buttons
		return true;
	},
	moveSatus: function() {
		return {moveId: Session.get('currentId')};
	}
});



Template.cmsList_slider.events({
	'click #listAdd': function(){											// Add post
		Session.set('addDialogShow', true);
	},
	'click #listDelete': function(){									// Delete
		if (this.classActive == 'enabled') {
			clientLog('Delete item');
			var r = confirm("Deze slide verwijderen?");
			if (r == true){
				Posts.remove( {_id: this.buttonId} );
			}
			// Posts.remove( {_id: this.buttonId} );
		}
	},
	'click .listBtn': function(){											// Select in list
		Session.set('currentId', this._id);
	},
	'click #btnListUp': function(){										// Move up
		listMove(this.moveId, -15);
	},
	'click #btnListDown': function(){									// Move down
		listMove(this.moveId, +15);
	}
});


