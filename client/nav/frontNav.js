// Wait till Route is rendered, then show frontNav /////////////

Template.frontNav.helpers({
	routeReady: function() {
		var state = (Session.get('language') == -1) ? false : true;
		return state;
	},
	buttons1: function() {
		return getNavListLevel1Front();
	},

	button1_1: function() {
		return getNavListLevel1FrontI(0);
	},
	button1_2: function() {
		return getNavListLevel1FrontI(1);
	},
	button1_3: function() {
		return getNavListLevel1FrontI(2);
	},
	button1_4: function() {
		return getNavListLevel1FrontI(3);
	},
	button1_5: function() {
		return getNavListLevel1FrontI(4);
	},
	button1_6: function() {
		return getNavListLevel1FrontI(5);
	},

	button1_row2: function() {
		return getNavListLevel1Front();
	}
});


Template.frontNav.events({
	'click .btn1': function(e) {									// Choose nav level 1
		e.preventDefault();
		Router.go( clickLevel1(this.index) );
	},
	'click .btn2': function(e) {									// Choose nav level 2
		e.preventDefault();
		var x = Session.get('navLevel1');
		nav.level2Last[x] = this.index;
		clientLog('choose level1 = ' + x + ' level2 = ' + this.index);
		Router.go( clickLevel2(this.index) );
	}
});
