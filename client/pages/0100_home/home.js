// home ////////////////////////////////////////////////////////

Template.home.helpers({
	item: function() {
		var l = Session.get('language');
		var p = 'home.path-nl'; 
		var g = '_mainGroup';
		var s = '_main';
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(post);
		return post;
	}
});
