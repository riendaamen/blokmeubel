
// home ////////////////////////////////////////////////////

Template.homeCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsHomePage', 				name: 'Pagina'},
			{component: 'cmsHomeSlider', 			name: 'Slider'},
			{component: 'cmsHomeHighlights', 	name: 'Highlights'}
		];
	}
});


Template.cmsHomePage.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'homeText',		name: 'Title'},
			{tabName: 'quote', 			name: 'Quote'},
			{tabName: 'seo', 				name: 'SEO'}
		];
	}
});


Template.cmsHomeSlider.helpers({
	group: function() {
		Session.set('currentPostGroup', 'slider');
	},
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'sliderTitle',	name: 'Title'},
			{tabName: 'sliderPhoto', 	name: 'Photo'}
		];
	}
});


Template.cmsHomeHighlights.helpers({
	group: function() {
		Session.set('currentPostGroup', 'highlight');
	},
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'highlight', 		name: 'Highlights'}
		];
	}
});


