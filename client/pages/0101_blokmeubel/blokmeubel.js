// blokmeubel ////////////////////////////////////////////////////////

Template.blokmeubel.helpers({
	item: function() {
		var l = Session.get('language');
		var p = 'blokmeubel.path-nl'; 
		var g = '_mainGroup';
		var s = '_main';
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(post);
		return post;
	},
	imageMaurice: function() {
		return ImagesPhoto.find( {'_id': this.image_maurice } );
	},
	imageAnke: function() {
		return ImagesPhoto.find( {'_id': this.image_anke } );
	}

	
});



