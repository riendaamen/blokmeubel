
// klanten ////////////////////////////////////////////////////

Template.klantenCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsKlantenPage', 				name: 'Pagina'},
			{component: 'cmsKlantenKlanten', 			name: 'Klanten'},
			{component: 'cmsKlantenArchitecten', 	name: 'Architecten'},
			{component: 'cmsKlantenOntwerpers', 	name: 'Ontwerpers'}
		];
	}
});



Template.cmsKlantenPage.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'quote', 		name: 'Quote'},
			{tabName: 'seo', 			name: 'SEO'}
		];
	}
});


Template.cmsKlantenKlanten.helpers({
	group: function() {
		Session.set('currentPostGroup', 'klanten');
	},
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'klantTitle', 		name: 'Title'},
			{tabName: 'klantPhoto', 		name: 'Photo'}
		];
	}
});


Template.cmsKlantenArchitecten.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'klantArchitecten', 		name: 'List'}
		];
	}
});


Template.cmsKlantenOntwerpers.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'klantOntwerpers', 		name: 'List'}
		];
	}
});



