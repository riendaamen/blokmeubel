// btn bestellen 	////////////////////////////////////////////////////////

Template.items_bestellen.helpers({
	showBestel: function() {
		var show = false;
		var p = getPageSlugBase().split('.')[0];
		// if (p=='meubels' || p=='tuinhaard') {
		if (p=='meubels') {
			var list = this.variationList;
			// clientLog('variationlist = ' + list.length);
			if (list.length > 0 ) {
				show = true;
			}
		}
		return show;
	},
	showBestel_tuinhaard: function() {
		var show = false;
		var p = getPageSlugBase().split('.')[0];
		if (p=='tuinhaard') {
			show = true;
		}
		return show;
	},
	showContact: function() {
		var show = false;
		var p = getPageSlugBase().split('.')[0];
		if (p=='interieur' || p=='verandas' || p=='objecten') {
			show = true;
		}
		return show;
	}
});




Template.items_bestellen.events({
	'click #btnBestelformulier': function(e){									// goto bestel-formulier
		e.preventDefault();
		clientLog ('goto bestel form');
		Router.go('bestelformulier', {item: this._id});
	},

	'click #btnBestelformulier_tuinhaard': function(e){				// goto bestel-formulier tuinhaard
		e.preventDefault();
		clientLog ('goto bestel form tuinhaard');
		Router.go('bestelformulier_tuinhaard', {item: this._id});
	},
	
	'click #btnContact': function(e){													// goto contact page
		e.preventDefault();
		clientLog ('goto contact page');
		Router.go('contact.path-nl');
	}
	
});


