// items  /main 		////////////////////////////////////////////////////////

Template.items.helpers({
	pageName: function() {
		return getPageSlugBase().split('.')[0];
	},

	showDetail: function() {
		var vis = true;
		var page = getPageSlugBase().split('.')[0];
		if (page == 'exposities' || page == 'publicaties') {
			if(this.detailShow == ''){
				vis = false;
			}
		}
		return vis;
	},
	page: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	},
	thumb: function() {
		Session.set('currentPostGroup', '_pagesGroup');
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var item = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		return item;
	},
	image: function() {
		return ImagesThumb.find( {'_id': this.image_01 } );
	}
});

Template.items.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});



// items_slug1  /main/x 		////////////////////////////////////////////////////////

Template.items_slug1.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_pagesGroup';
		var s = Session.get('navParam1');
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	}
});

Template.items_slug1.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName);
	}
});
