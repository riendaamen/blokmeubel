
// items ////////////////////////////////////////////////////

Template.itemsCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsItemsProducts', 	name: 'Items'},
			{component: 'cmsItemsPage', 		name: 'Pagina'}
		];
	}
});


Template.cmsItemsPage.helpers({
	editTabs: function() {
		// clientLog('slugbase = ' + getPageSlugBase() );
		nav.editTabs = [
			{tabName: 'itemText', 	name: 'Text'},
			{tabName: 'quote', 		name: 'Quote'},
			{tabName: 'seo', 		name: 'SEO'}
		];
		if ( (getPageSlugBase() == 'exposities.path-nl') || (getPageSlugBase() == 'publicaties.path-nl') ){
			nav.editTabs = [
				{tabName: 'quote', 		name: 'Quote'},
				{tabName: 'seo', 		name: 'SEO'}
			];
		}
	}
});


Template.cmsItemsProducts.helpers({
	editTabs: function() {
		var page = getPageSlugBase().split('.')[0];
		nav.editTabs = [
			{tabName: 'itemProduct', 				name: 'Front'},
			{tabName: 'itemPhoto', 					name: 'Photo'},
			{tabName: 'itemProductDetail', 	name: 'Detail'},
			{tabName: 'itemSlides', 				name: 'Slides'},
			{tabName: 'seo', 								name: 'SEO'}
		];

		if ( page == 'exposities' || page == 'publicaties'){
			nav.editTabs = [
				{tabName: 'itemFront', 					name: 'Front'},
				{tabName: 'itemPhoto', 					name: 'Photo'},
				{tabName: 'itemProductDetail', 	name: 'Detail'},
				{tabName: 'itemSlides', 				name: 'Slides'},
				{tabName: 'seo', 								name: 'SEO'}
			];
		}

		// if ( page == 'meubels' || page == 'tuinhaard' ){
		if ( page == 'tuinhaard' ){
			nav.editTabs = [
				{tabName: 'itemVariations_th',	name: 'Uitv. tuinhaard'},

				{tabName: 'itemProduct', 		name: 'Front'},
				// {tabName: 'itemFront', 	name: 'Front'},
				{tabName: 'itemPhoto', 			name: 'Photo'},
				{tabName: 'itemDetail', 		name: 'Detail'},
				// {tabName: 'itemVariations',	name: 'Uitvoeringen'},

				{tabName: 'itemSlides', 		name: 'Slides'},
				{tabName: 'seo', 						name: 'SEO'}
			];
		}
	}
});
