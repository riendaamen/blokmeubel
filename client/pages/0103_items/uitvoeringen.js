
Template.uitvoeringen.helpers({

	show_meubels: function() {
		var page = getPageSlugBase().split('.')[0];
		var vis = false;
		// if (page == 'meubels' || page == 'tuinhaard') {
		if (page == 'meubels') {
			var list = this.variationList;
			// clientLog('variationlist = ' + list.length);
			if (list.length > 0 ) {
				vis = true;
			}
		}
		return vis;
	},

	variation: function() {
		var list = this.variationList;
		var slides = [];
		for (var i = 0; i < list.length; i++) {
			var slide = [];
			slide.index = 	i;
			slide.name = 		list[i].name;
			slide.price = 	list[i].price;
			// slide.active = (i == Session.get('currentVariation')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},



	show_tuinhaard: function() {
		var page = getPageSlugBase().split('.')[0];
		var vis = false;
		if (page == 'tuinhaard') {
			var list = this.variationList;
			// clientLog('variationlist = ' + list.length);
			// if (list.length > 0 ) {
				vis = true;
			// }
		}
		return vis;
	}





});
