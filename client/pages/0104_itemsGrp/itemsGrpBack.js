// itemsGrp ////////////////////////////////////////////////////

Template.itemsGrpCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsItemsGrpPage', 		name: 'Pagina'},
			{component: 'cmsItemsGrpGroups', 	name: 'Groups'},
			{component: 'cmsItemsGrpProducts', 	name: 'Items'}
		];
	}
});


Template.cmsItemsGrpPage.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'itemText', name: 'Text'},
			{tabName: 'quote', 		name: 'Quote'},
			{tabName: 'seo', 			name: 'SEO'}
		];
	}
});


Template.cmsItemsGrpGroups.helpers({
	editTabs: function() {
		// clientLog('slugbase = ' + getPageSlugBase() );
		nav.editTabs = [
			{tabName: 'groupText', 	name: 'Text'},
			{tabName: 'itemPhoto', 	name: 'Photo'},
			{tabName: 'seo', 		name: 'SEO'}
		];
	}
});


Template.cmsItemsGrpProducts.helpers({
	editTabs: function() {
		var page = getPageSlugBase().split('.')[0];
		nav.editTabs = [
			{tabName: 'itemProduct', 			name: 'Front'},
			{tabName: 'itemPhoto', 				name: 'Photo'},
			{tabName: 'itemProductDetail', 		name: 'Detail'},
			{tabName: 'itemSlides', 			name: 'Slides'},
			{tabName: 'seo', 					name: 'SEO'}
		];
		if ( page == 'meubels' ){
			nav.editTabs = [
				{tabName: 'itemProduct', 			name: 'Front'},
				{tabName: 'itemPhoto', 			name: 'Photo'},
				{tabName: 'itemDetail', 		name: 'Detail'},
				{tabName: 'itemVariations',		name: 'Uitvoeringen'},
				{tabName: 'itemSlides', 		name: 'Slides'},
				{tabName: 'seo', 				name: 'SEO'}
			];
		}

	}
});
