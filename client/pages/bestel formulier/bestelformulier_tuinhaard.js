// still needs honeypot

// form validation ////////////////////////////////////////////////////////////////
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

// bestelformulier_tuinhaard ////////////////////////////////////////////////////////
Template.bestelformulier_tuinhaard.rendered = function(){
	Session.set('notification_name', '');
	Session.set('notification_email', '');
	Session.set('notification_phone', '');
	Session.set('notification_terms', '');
	Session.set('bestelform_variation', 0);
	Session.set('bestelform_amount', 1);
};


Template.bestelformulier_tuinhaard.helpers({
	notification_name: function() {
		return Session.get('notification_name');
	},
	notification_email: function() {
		return Session.get('notification_email');
	},
	notification_phone: function() {
		return Session.get('notification_phone');
	},
	notification_terms: function() {
		return Session.get('notification_terms');
	},
	item: function() {
		var query = Posts.find({'_id': nav.param1});
		return query;
	},
	size: function() {
		var list = this.sizesList;
		var count = this.sizesListCount;
		var slides = [];
		for (var i = 0; i < count; i++) {
			var slide = [];
			slide.index = 	i;
			slide.name = 		list[i].name;
			slide.price = 	list[i].price;
			slide.active = (i == Session.get('currentVariation')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	option: function() {
		var list = this.variationList;
		var slides = [];
		for (var i = 0; i < list.length; i++) {
			var slide = [];
			slide.index = 	i;
			slide.name = 		list[i].name;
			slide.price = 	list[i].price;
			slide.active = (i == Session.get('currentVariation')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	price_A: function() {
		Session.get('test');
		var price = parseFloat(this.sizesList[Session.get('bestelform_variation')].price);
		// clientLog ('price = ' + price );
		var list = this.variationList;
		for (var i = 0; i < list.length; i++) {
			var val = $('#checkbox'+i).is(':checked');
			// clientLog ('option ' +  i + ': ' + list[i].name + ':' +  val);
			if (val) {
				price = price + parseFloat(list[i].price);
			}
		}
		//var amount = Session.get('bestelform_amount');
		// return (price*amount).toFixed(2);
		// clientLog ('price = ' + price );
		return (price).toFixed(2);
		// return (price);
	},
	price_B: function() {
		Session.get('test');
		var price = parseFloat(this.sizesList[Session.get('bestelform_variation')].price);
		var list = this.variationList;
		for (var i = 0; i < list.length; i++) {
			var val = $('#checkbox'+i).is(':checked');
			if (val) {
				price = price + parseFloat(list[i].price);
			}
		}
		price = price + parseFloat(50.00);
		// return (price);
		return (price).toFixed(2);
	}
});


Template.bestelformulier_tuinhaard.events ({
	'change #selectVariation': function(event, template){							// select size
		var num = $('#selectVariation option:selected').data('num');
		Session.set('bestelform_variation', num);
		// clientLog ('select variation = ' + num );
	},
	'change .optionInputs': function(event, template){								// select option on/off
		clientLog('change options');
		Session.set('test', !Session.get('test'));
	},
	'click #submit': function(e){																			// form request submit
		e.preventDefault();
		var notification;

		var name_valid = ($('#achternaam').val() !== '') ? true : false;
		notification = (name_valid) ? '' : 'Graag je naam invullen.';
		Session.set('notification_name', notification);

		var email_valid = (!isValidEmailAddress( $('#email').val() )) ? false : true;
		notification = (email_valid) ? '' : 'Het e-mailadres ontbreekt of klopt niet.';
		Session.set('notification_email', notification);

		var phone_valid = ($('#telefoon-mobiel').val() !== '') ? true : false;
		notification = (phone_valid) ? '' : 'Het telefoon-nummer ontbreekt.';
		Session.set('notification_phone', notification);

		var terms_valid = ( $('input[name="terms"]:checked').val() == 'accept') ? true : false;
		notification = (terms_valid) ? '' : 'U dient akkoord te gaan met de voorwaarden.';
		Session.set('notification_terms', notification);


		if (name_valid && email_valid && phone_valid && terms_valid) {					// form is valid
		// if (true) {								// form is valid

			var query = Posts.find({'_id': nav.param1}).fetch()[0];

			// size
			// var size = parseFloat(this.sizesList[Session.get('bestelform_variation')].name);
			var size = query.sizesList[Session.get('bestelform_variation')].name;

			// options
			// var variant = query.variationList[Session.get('bestelform_variation')].name;
			var options = '';
			// var list = this.variationList;
			var list = query.variationList;
			for (var i = 0; i < list.length; i++) {
				var val = $('#checkbox'+i).is(':checked');
				if (val) {
					options = options + list[i].name + ', ';
				}
			}

			// price
			var price = parseFloat(query.sizesList[Session.get('bestelform_variation')].price);
			var list = query.variationList;
			for (var i = 0; i < list.length; i++) {
				var val = $('#checkbox'+i).is(':checked');
				if (val) {
					price = price + parseFloat(list[i].price);
				}
			}
			price = price + parseFloat(50.00);
			price = (price).toFixed(2);



			var m = '';
			var c = "<tr><td style='color:#C7B296; width: 40%;'>"

			m = m + "<table border='0' style='width:520px; color: #53473F; font-size: 12px; border-spacing: 2px; padding: 14px; margin: 10px; line-height: 20px; font-family:verdana; border: 0px;'>";
			m = m + "<caption style='font-weight: bold; text-align: left; margin-left: 18px;'>Bestelformulier</caption>";

			m = m + c + 'naam product' 				+ '</td><td>' 	+ query.title 							+ '</td></tr>';
			m = m + c + 'product page: '			+ '</td><td>' 	+ query.page.split('.')[0] 	+ '</td></tr>';
			if(query.groupSlug !== '_pagesGroup'){
				m = m + c + 'product group: '		+ '</td><td>' 	+ query.groupName						+ '</td></tr>';
			}
			m = m + c + 'size: '															+ '</td><td>'+ size			 		+ '</td></tr>';
			m = m + c + 'options: '														+ '</td><td>'+ options			+ '</td></tr>';
			m = m + c + 'totaal prijs incl. verzendkosten: '	+ '</td><td>'+ price				+ '</td></tr>';

			m = m + c + '-'																		+ '</td><td>'+ '-'					+ '</td></tr>';

			m = m + c + 'voornaam: '				+ '</td><td>' + $('#voornaam').val() 					+ '</td></tr>';
			m = m + c + 'tussenvoegsel: '		+ '</td><td>' + $('#tussenvoegsel').val() 		+ '</td></tr>';
			m = m + c + 'achternaam: '			+ '</td><td>' + $('#achternaam').val() 				+ '</td></tr>';
			m = m + c + 'straatnaam: '			+ '</td><td>' + $('#straatnaam').val() 				+ '</td></tr>';
			m = m + c + 'huisnummer: '			+ '</td><td>' + $('#huisnummer').val() 				+ '</td></tr>';
			m = m + c + 'postcode: '				+ '</td><td>' + $('#postcode').val() 					+ '</td></tr>';
			m = m + c + 'stad: '						+ '</td><td>' + $('#stad').val() 							+ '</td></tr>';
			m = m + c + 'email: '						+ '</td><td>' + $('#email').val() 						+ '</td></tr>';
			m = m + c + 'telefoon-mobiel: '	+ '</td><td>' + $('#telefoon-mobiel').val() 	+ '</td></tr>';
			m = m + c + 'opmerkingen: '			+ '</td><td>' + $('#opmerkingen').val() 			+ '</td></tr>';
			m = m + c + 'levering: '				+ '</td><td>' + $('input[name="bestelling"]:checked').val()  	+ '</td></tr>';

			m = m + "</table>"



			var email = $('#email').val();
			clientLog('send email: ' + m);
			Meteor.call( 'emailFeedback', email, 'Blok bestelformulier tuinhaard', m );		// send email to blokmeubel
			Meteor.call( 'emailCustomer', email );																				// send email to user

			Session.set('notification_name', '');
			Session.set('notification_email', '');
			Session.set('notification_phone', '');
			Session.set('notification_terms', '');

			$('#voornaam').val('');
			$('#tussenvoegsel').val('');
			$('#achternaam').val('');
			$('#straatnaam').val('');
			$('#huisnummer').val('');
			$('#postcode').val('');
			$('#stad').val('');
			$('#email').val('');
			$('#telefoon-mobiel').val('');
			$('#opmerkingen').val('');

			Router.go('bedankt');

		}
	}

});
