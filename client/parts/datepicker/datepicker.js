// Datepicker /////////////////////////////////////////////////////////

Template.cmsPikaday.rendered = function() {
	picker = new Pikaday({
		field: document.getElementById('datepicker'),
		trigger: document.getElementById('datepickerBtn'),
		format: 'D MMM YYYY',
		onSelect: function() {
			Posts.update( {'_id': Session.get('currentId')}, {$set: {
				'date': 			this.getDate(),
				'dateNice': 	dateNice(this.getDate())
			}});
		}
	});
}


function dateNice(date) {
	var y = date.getFullYear();
	var d = date.getDate();
	var month=new Array();
	month[0]="januari";
	month[1]="februari";
	month[2]="maart";
	month[3]="april";
	month[4]="mei";
	month[5]="juni";
	month[6]="juli";
	month[7]="augustus";
	month[8]="september";
	month[9]="oktober";
	month[10]="november";
	month[11]="december";
	var m = month[date.getMonth()]; 
	var nice = d + ' ' + m + ' ' + y;
	return nice;
}


Template.cmsPikaday.helpers({
	dateYear: function() {
		return this.date.getFullYear();
	},
	dateMonth: function() {
		return this.date.getMonth()+1;
	},
	dateDay: function() {
		return this.date.getDate();
	}	
});


