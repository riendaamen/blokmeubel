
// Footer ////////////////////////////////////////////////////////

Template.footer.helpers({
	footerInfo: function() {
		var str = ''
		var str = str + getPageSlugBase() + ' - ';
		var str = str + nav.param1 + ' - ';
		var str = str + nav.param2 + ' - ';
		return str;
	},
	footerSwitch: function() {
		var p = getPageSlugBase();
		var r = p.split('.')[0];
		// clientLog('footer, root.page = ' + r);

		var value = 0;				//	0=quote		1=textMain		2=textGroup

		if (r=='blokmeubel') {
			value = 1;
		}
		if (r=='tuinhaard' || r=='verandas' || r=='kunst-openbare-ruimte') {
			if (nav.param1=='') {
				value = 1;
			}
		}
		if (r=='interieur' || r=='meubels') {
			if (nav.param2=='') {
				value = 2;
			}
			if (nav.param1=='' && nav.param2=='') {
				value = 1;
			}
		}
		if (r=='interieur') {
			if (nav.param1=='' && nav.param2=='') {
				value = 0;
			}
		}
		return value;
	}
});


Template.footerQuote.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		// clientLog('footer, page = ' + p);
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s} );
		return query;
	},
	footerQuoteLink: function() {
		return (getPageSlugBase().split('.')[0]=='home') ? true : false;
	}
});


Template.footerTextMain.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s} );
		return query;
	}
});


Template.footerTextGroup.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_groupsGroup';
		var s = nav.param1;
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s} );
		return query;
	}
});


Template.footerNAW.events({
	'click #btnSitemap': function(e){									// goto sitemap
		e.preventDefault();
		clientLog ('goto sitemap');
		Router.go('sitemap');
	}
});
