// blogGroups Nav bar	///////////////////////////////////////////////////////////////////////////////////
// 		used for dualslug, but also for normal singleslugposts
//  	in that case it doesn't show, and grp = _none
//

Template.cmsGroupNav.helpers({
	list: function() {
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var ret = Posts.find( {'page': p, 'language': l, 'groupSlug': '_groupsGroup'}, {sort: {timeStamp: 1}} );
		return ret;
	},
	listSelected: function() {
		return Session.equals('currentBlogGroupId', this._id) ? 'active' : '';
	},


	checkSelected: function() {
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var q = Posts.find( {_id: Session.get('currentBlogGroupId'), page: p, groupSlug: '_groupsGroup', language: l} );

		if (q.count() == 0){ 		// No result
			q = Posts.find( {page: p, groupSlug: '_groupsGroup', language: l} );
			Session.set('currentBlogGroupId', q.fetch()[0]._id);
		}

		return;
	}

});

Template.cmsGroupNav.events({
	'click .listBtn': function(){												// Select group
		Session.set('currentBlogGroupId', this._id);
		Session.set('currentId', this._id);
	}
});



// Get current post group //////////////////////////////////////////////////////////////////////////////
// 		check if there is a group uberhaupt
//
getPostGroupName = function() {
	return ('__temp_groupname');
}

getPostGroupSlug = function() {
	return (Session.get('currentPostGroup'));
}
