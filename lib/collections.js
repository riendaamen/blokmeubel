Posts = new Mongo.Collection('posts');

Posts.allow({
	insert: function(userId, doc) {
		return !! userId;				// only allow insert if you are logged in
	},
	update: function(userId, doc) {
		return !! userId;				// only allow update if you are logged in
	},
	remove: function(userId, doc) {
		return !! userId;				// only allow insert if you are logged in
	}
});


