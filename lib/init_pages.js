// Pages in Nav and URL
//
// 	urlName			URL															[language]
// 	btnName			Nav button level1 and level2  	[language]
//	pathType		normal, singleSlug, doubleSlug, query
//	template			template in client/pages/
//								contains front and cms template
//								cms template holds cmsTabs


pages = 
	[
		{	urlName: ['home', 'home-en'],
			btnName: ['Home', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'normal',
				 template: 'home'}  
				 // template: 'boilerplate'}  
			]
		},
		{	urlName: ['blokmeubel', 'blokmeubel-en'],
			btnName: ['Blokmeubel', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'normal',
				 template: 'blokmeubel'}  
			]
		},
		{	urlName: ['klanten', 'klanten-en'],
			btnName: ['Klanten', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'normal',
				 template: 'klanten'}  
			]
		},
		{	urlName: ['exposities', 'exposities-en'],
			btnName: ['Exposities', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'singleSlug',
				 template: 'items'}  
			]
		},
		{	urlName: ['publicaties', 'publicaties-en'],
			btnName: ['Publicaties', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'singleSlug',
				 template: 'items'}  
			]
		},
		{	urlName: ['contact', 'contact-en'],
			btnName: ['Contact', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'normal',
				 template: 'contact'}  
			]
		},


		{	urlName: ['interieur', 'interieur-en'],
			btnName: ['Interieur', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'doubleSlug',
				 template: 'itemsGrp'}  
			]
		},
		{	urlName: ['meubels', 'meubels-en'],
			btnName: ['Meubels', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'doubleSlug',
				 template: 'itemsGrp'}  
			]
		},
		{	urlName: ['tuinhaard', 'tuinhaard-en'],
			btnName: ['Tuinhaard', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'singleSlug',
				 template: 'items'}  
			]
		},
		{	urlName: ['verandas', 'verandas-en'],
			btnName: ["Veranda's", 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'singleSlug',
				 template: 'items'}  
			]
		},
		{	urlName: ['kunst-openbare-ruimte', 'kunst-openbare-ruimte-en'],
			btnName: ['Objecten', 'Name EN'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'singleSlug',
				 template: 'items'}  
			]
		}
		
	]

	
	

// pages = 
	// [
		// {	urlName: ['home', 'home'],
			// btnName: ['home', 'home'],
			// level2: [
				// {urlName: ['path-nl', 'path-en'],
				 // btnName: ['btn name NL', 'btn name EN'],
				 // pathType: 'normal',
				 // template: 'home'}  
			// ]
		// },
		
		// {	urlName: ['coaching', 'coaching'],
		 	// btnName: ['coaching', 'coaching'],
			// level2: [
				// {urlName: ['individuele-begeleiding', 'individual-support'],
				 // btnName: ['coaching', 'coaching'],
				 // pathType: 'singleSlug',
				 // template: 'blog'},
				// {urlName: ['wanneer-coaching', 'when-coaching'],
				 // btnName: ['wanneer coaching', 'when coaching'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['coachtraject', 'coaching-program'],
				 // btnName: ['coachtraject', 'coaching program'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['verantwoordelijkheid', 'responsibility'],
				 // btnName: ['verantwoordelijkheid', 'responsibility'],
				 // pathType: 'normal',
				 // template: 'main'}
			// ]
		// },

		// {	urlName: ['cursussen', 'courses'],
		 	// btnName: ['workshops', 'workshops'],
			// level2: [
				// {urlName: ['workshops', 'workshops'],
				 // btnName: ['cursussen & workshops', 'courses & workshops'],
				 // pathType: 'singleSlug',
				 // template: 'blog'}
			// ]
		// },
		
		// {	urlName: ['meditatie', 'meditation'],
		 	// btnName: ['meditatie', 'meditation'],
			// level2: [
				// {urlName: ['technieken', 'techniques'],
				 // btnName: ['meditatie', 'meditation'],
				 // pathType: 'normal',
				 // template: 'main'}
			// ]
		// },
		
		// {	urlName: ['inspiratie', 'inspiration'],
		 	// btnName: ['inspiratie', 'inspiration'],
			// level2: [
				// {urlName: ['samenkomst', 'meeting'],
				 // btnName: ['samenkomst', 'meeting'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['lezingen', 'lectures'],
				 // btnName: ['lezingen', 'lectures'],
				 // pathType: 'normal',
				 // template: 'extLinks'},						// needs to be links to pdf/url
				// {urlName: ['om-te-delen', 'sharing'],
				 // btnName: ['om te delen', 'sharing'],
				 // pathType: 'doubleSlug',
				 // template: 'sharing'}							// needs to be muziek/boeken etc
			// ]
		// },
		
		// {	urlName: ['one-sense', 'one-sense'],
		 	// btnName: ['over ons', 'about us'],
			// level2: [
				// {urlName: ['bewustwording', 'awareness'],
				 // btnName: ['one sense', 'one sense'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['francoise', 'francoise'],
				 // btnName: ['francoise', 'francoise'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['gilbert', 'gilbert'],
				 // btnName: ['gilbert', 'gilbert'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['visie', 'vision'],
				 // btnName: ['visie', 'vision'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['onze-werkwijze', 'how-we-work'],
				 // btnName: ['onze werkwijze', 'how we work'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['anderen-over-ons', 'others-about-us'],
				 // btnName: ['anderen over ons', 'others about us'],
				 // pathType: 'doubleSlug',
				 // template: 'blogGroup'}
			// ]
		// },

		// {	urlName: ['info', 'info'],
		 	// btnName: ['info', 'info'],
			// level2: [
				// {urlName: ['tarieven-en-prijzen', 'rates-and-prices'],
				 // btnName: ['tarieven en prijzen', 'rates and prices'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['annulering', 'cancellation'],
				 // btnName: ['annulering', 'cancellation'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['folders', 'leaflets'],
				 // btnName: ['folders', 'leaflets'],
				 // pathType: 'normal',
				 // template: 'extLinks'},		// needs to be links to pdf/url
				// {urlName: ['geschenkbon', 'gift-voucher'],
				 // btnName: ['geschenkbon', 'gift voucher'],
				 // pathType: 'normal',
				 // template: 'main'},
				// {urlName: ['links', 'links'],
				 // btnName: ['links', 'links'],
				 // pathType: 'normal',
				 // template: 'main'}
			// ]
		// },

		// {	urlName: ['actueel', 'up-to-date'],
		 	// btnName: ['actueel', 'up to date'],
			// level2: [
				// {urlName: ['agenda', 'events'],
				 // btnName: ['agenda', 'events'],
				 // pathType: 'singleSlug',
				 // template: 'agenda'},						// agenda nl+en
				// {urlName: ['sense-talk', 'sense-talk'],
				 // btnName: ['sense-talk', 'sense-talk'],
				 // pathType: 'singleSlug',
				 // template: 'sensetalk'},				
				// {urlName: ['nieuwsbrief', 'newsletter'],
				 // btnName: ['nieuwsbrief', 'newsletter'],
				 // pathType: 'normal',
				 // template: 'extLinks'}					// links
			// ]
		// },

		// {	urlName: ['contact', 'contact'],
		 	// btnName: ['contact', 'contact'],
			// level2: [
				// {urlName: ['gegevens', 'information'],
				 // btnName: ['contactgegevens', 'contact information'],
				 // pathType: 'normal',
				 // template: 'main'},				
				// {urlName: ['plan-je-route', 'plan-your-route'],
				 // btnName: ['plan je route', 'plan your route'],
				 // pathType: 'normal',
				 // template: 'main'}							// google map
			// ]
		// },
		
		// {	urlName: ['extra', 'extra'],			// hide in front nav
		 	// btnName: ['extra', 'extra'],
			// level2: [
				// {urlName: ['pagina', 'page'],
				 // btnName: ['pagina', 'page'],
				 // pathType: 'singleSlug',
				 // template: 'blog'}
			// ]
		// }
		
	// ]

	
	
