languages = [
	{	nameLong: 'Nederlands',
		name: 'nl',
		path: ''
	}
];


// languages = [
	// {	nameLong: 'Nederlands',
		// name: 'nl',
		// path: ''
	// },
	// {	nameLong: 'English',
		// name: 'en',
		// path: 'en'
	// }
// ];




//
// Check initial language for slug URL
// 	this is needed when user lands on post before language is defined by other pages
//
checkLanguageSlug = function() {
	var language = Session.get('language');
	if (language == -1) {
		Session.set('language', 0);
	}
}
