// Variables
//------------------------------------------------------------------------------------------------------------------------------------------------
//...


// Default Values
//------------------------------------------------------------------------------------------------------------------------------------------------


// Functions
//------------------------------------------------------------------------------------------------------------------------------------------------
// Opacity change on hover, trigger and element
function opacityHoverTriggerInner (trigger, element, opacityOn, opacityOff, speed) {
	$(trigger).hover(function() {
		$(this).find(element).stop().animate({'opacity': opacityOn}, speed);
		},function() {
		$(this).find(element).stop().animate({'opacity': opacityOff}, speed);
	});
}

// Color change on hover text
function fontColorHover (item, colorDefault, colorHover, speed) {
	$(item).hover(function() {
		$(this).stop().animate({ color: colorHover }, speed);
		},function() {
		$(this).stop().animate({ color: colorDefault }, speed);
	});
}

// Background color change on hover text
function btnHover (item, color1, color2, speed) {
	$(item).hover(function() {
		$(this).stop().animate({ backgroundColor: color2, color: color1 }, speed);
		},function() {
		$(this).stop().animate({ backgroundColor: color1, color: color2 }, speed);
	});
}

// Color change on hover text, trigger and element
function fontColorHoverTrigger (trigger, element, colorDefault, colorHover, speed) {
	$(trigger).hover(function() {
		$(this).find(element).stop().animate({ color: colorHover }, speed);
		},function() {
		$(this).find(element).stop().animate({ color: colorDefault }, speed);
	});
}

function submitenter(myfield,e) {
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;

	if (keycode == 13) {
		myfield.form.submit();
		return false;
	}
	else
	return true;
}

function clearDefault(el) { if (el.defaultValue==el.value) el.value = ""; }

function loseFocus(inputEl) {
	if (inputEl.value === "") {
		inputEl.value = inputEl.defaultValue;
	}
}


// On doc ready
//------------------------------------------------------------------------------------------------------------------------------------------------
$(document).ready(function(){
	// effects
	fontColorHover('.menuHover', "#534741", "#C7B299", 50);
	fontColorHoverTrigger('.over_teamMember_mail', '.emailHover', "#FFFFFF", "#736357", 50);
	opacityHoverTriggerInner('.home_cont_product', '.hover', '0.25', '0', 200);
	opacityHoverTriggerInner('.over_opdrg_bedr_logo', '.hover', '0.7', '0', 200);
	opacityHoverTriggerInner('.over_teamMember_mail', '.hover', '1', '0', 100);
	opacityHoverTriggerInner('.home_cont_sliderBtn', '.hover', '1', '0', 100);
	opacityHoverTriggerInner('.btmFooter #panel4 #social1', 'span', '0', '1', 100);
	opacityHoverTriggerInner('.btmFooter #panel4 #social2', 'span', '0', '1', 100);
	fontColorHoverTrigger('.home_cont_sliderBtn', '.text', "#FFFFFF", "#534741", 50);
	fontColorHover('.submenuLink_hover', '#534741', '#998675', 50);

	btnHover('.shopBtn', '#534741', '#FFFFFF', 50);

	// sudo slider
	var sudoSlider = $("#home_cont_sliderImg").sudoSlider({
		numeric:		true,
		prevnext:		false,
		auto:			true,
		ease:			'swing',
		speed:			200,
		pause:			'4000'
	});

	// sudo slider
	var sudoSlider = $("#shopDetail_slider_img").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});

	// submenu slide to
	$('#sub_werk1').click(function() { $('html,body').stop().animate({ scrollTop: $('#over_team_cont').offset().top - 200 }, 450, 'easeOutQuad'); });
	$('#sub_werk2').click(function() { $('html,body').stop().animate({ scrollTop: $('#over_cont2').offset().top - 200 }, 450, 'easeOutQuad'); });
	$('#sub_werk3').click(function() { $('html,body').stop().animate({ scrollTop: $('#over_opdrg_cont').offset().top - 200 }, 450, 'easeOutQuad'); });

	// sudo slider
	var sudoSlider2 = $("#werk_cont_sliderImg1").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider3 = $("#werk_cont_sliderImg2").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider4 = $("#werk_cont_sliderImg3").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider5 = $("#werk_cont_sliderImg4").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider6 = $("#werk_cont_sliderImg5").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider7 = $("#werk_cont_sliderImg6").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider8 = $("#werk_cont_sliderImg7").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider9 = $("#werk_cont_sliderImg8").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider10 = $("#werk_cont_sliderImg9").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider11 = $("#werk_cont_sliderImg10").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider12 = $("#werk_cont_sliderImg11").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider13 = $("#werk_cont_sliderImg12").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider14 = $("#werk_cont_sliderImg13").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider15 = $("#werk_cont_sliderImg14").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider16 = $("#werk_cont_sliderImg15").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider17 = $("#werk_cont_sliderImg16").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider18 = $("#werk_cont_sliderImg17").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider19 = $("#werk_cont_sliderImg18").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider20 = $("#werk_cont_sliderImg19").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider21 = $("#werk_cont_sliderImg20").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider22 = $("#werk_cont_sliderImg21").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider23 = $("#werk_cont_sliderImg22").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider24 = $("#werk_cont_sliderImg23").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider25 = $("#werk_cont_sliderImg24").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider26 = $("#werk_cont_sliderImg25").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider27 = $("#werk_cont_sliderImg26").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider28 = $("#werk_cont_sliderImg27").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider29 = $("#werk_cont_sliderImg28").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider30 = $("#werk_cont_sliderImg29").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
	var sudoSlider31 = $("#werk_cont_sliderImg30").sudoSlider({numeric:true,prevnext:false,ease:'swing',speed:300});
});