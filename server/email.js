Meteor.methods({
	mailtest: function () {
		console.log('RD: mailtest.');
		return 'ok';
	},
	emailFeedback: function (from, subject, body) {
		console.log('RD: sending email...');
		this.unblock();		// Don't wait for result
		// var postURL = process.env.MAILGUN_API_URL + '/' + process.env.MAILGUN_DOMAIN + '/messages';
		var postURL = 'https://api.mailgun.net/v2' + '/' + 'heavymedia.nl' + '/messages';
		var options = {
			auth: "api:" + 'key-4cb557dfc87898308cbfb95932be',
			params: {
				"from":				from,
				"to":					['orders@blokmeubel.nl'],
				"subject": 		subject,
				"html": 			body
			}
		}
		var onError = function(error, result) {
			if(error) {console.log("Error: " + error)}
		}
		// Send the request
		Meteor.http.post(postURL, options, onError);
		console.log('RD: Email feedback sent');
	},
	emailCustomer: function (to) {
		console.log('RD: sending email...');
		this.unblock();		// Don't wait for result
		var body = ""
		body = body + "Dank je wel voor de bestelling. <br>";
		body = body + "We zullen spoedig contact met u opnemen. <br>";
		body = body + "<br>";
		body = body + "met vriendelijke groet, blokmeubel <br>";
		// var postURL = process.env.MAILGUN_API_URL + '/' + process.env.MAILGUN_DOMAIN + '/messages';
		var postURL = 'https://api.mailgun.net/v2' + '/' + 'heavymedia.nl' + '/messages';
		var options = {
			auth: "api:" + 'key-4cb557dfc87898308cbfb95932be',
			params: {
				"from":				'info@blokmeubel.nl',
				"to":					[to],
				"subject": 		'Blokmeubel, bevestiging bestel-formulier.',
				"html": 			body
			}
		}
		var onError = function(error, result) {
			if(error) {console.log("Error: " + error)}
		}
		// Send the request
		Meteor.http.post(postURL, options, onError);
		console.log('RD: email customer sent');
	}
});
